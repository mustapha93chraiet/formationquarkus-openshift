package com.madadipouya.quarkus.example.service;

import com.madadipouya.quarkus.example.entity.Ruche;

import java.util.List;

public interface RucheService {

    List<Ruche> getAllRuches();
}
